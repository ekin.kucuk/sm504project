# SM504Project

## Technology Requirements

1. Java 1.8 
   Download link: https://www.oracle.com/tr/java/technologies/javase/javase8-archive-downloads.html
2. Npm, Node
   Download link: https://nodejs.org/en/download/
3. PostgreSQl
   Download link: https://www.postgresql.org/download/


## Source Codes

### Back-End
https://gitlab.com/ekin.kucuk/sm504projectserver


### Front-End
https://gitlab.com/ekin.kucuk/sm504projectclient

## Running locally 

### Front-End
After cloning the project, go to the project directory and open a terminal to run:
1. npm install
2. npm run 

After the execution the project should be running

### Back-End 

After cloning the project, please run the application from an ide 

make sure that your postgresql is run with the below crendatials 

localhost:5432/postgres
username= postgres
password= root
